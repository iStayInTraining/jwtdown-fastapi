### v0.3.0

* Added expiration for JWTs in Authenticator
  * Default is one hour
  * Can customize using exp parameter in initializer
  * Can override on a per JWT basis using get_exp method
* All tokens now have a `"jti"` claim that can be used for
  immediate token revocation

### v0.2.0

* First public version

### v0.1.6

* Getting the build artifacts to PyPi, again.

### v0.1.5

* Handle build artifacts smarter.
* Handle build caches smarter.

### v0.1.3

* Build and deploy Read The Docs site.

### v0.1.2

* Publish build artifacts to PyPi.

### v0.1.1

* Build artifacts only on tags
* Test all pushes

### v0.1.0

* Initial import.
